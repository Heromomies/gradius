﻿using System;
using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;

public class JumpVide : MonoBehaviour
{
    [SerializeField]
    public float moveSpeed = 5f;

    [SerializeField]
    public float frequency = 20f;

    [SerializeField] 
    public float magnitude = 0.5f;

    public Vector3 pos, localScale;

    public int score, health;
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
        localScale = transform.localScale;
        health = 1;
    }
    public void Update()
    {
        MoveLeft();
        if (health <= 0)
        {
            Die();
        }
    }
    public void MoveRight() // ajoute un mouvement sinusoïdal à l'ennemi vers la droite
    {
        pos += transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
    public void MoveLeft() // ajoute un mouvement sinusoïdal à l'ennemi vers la gauche
    {
        pos -= transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
    public void Damage()
    {
        health--;
    }
    public void Die()
    {
        PlayerPrefs.SetInt("Score", PlayerPrefs.GetInt("Score") + score);
    }
}
