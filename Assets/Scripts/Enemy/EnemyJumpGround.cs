﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyJumpGround : MonoBehaviour
{
    [SerializeField]
    public float moveSpeed = 5f;

    [SerializeField]
    public float frequency = 20f;

    [SerializeField] 
    public float magnitude = 0.5f;

    public bool facingRight;
    public Vector3 pos, localScale;
    
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
        localScale = transform.localScale;
    }

    public void Update()
    {
        CheckWhereToFace();

        if (facingRight == true)
        {
            MoveRight();
        }
        else
        {
            MoveLeft();
        }
    }

    public void CheckWhereToFace()
    {
        if (pos.x < -7f)
        {
            facingRight = true;
        }
        else if (pos.x > 7f)
        {
            facingRight = false;
        }
        if(((facingRight)&& (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
        {
            localScale.x *= -1;
        }
        transform.localScale = localScale;
    }

    void MoveRight()
    {
        pos += transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
    void MoveLeft()
    {
        pos -= transform.right * Time.deltaTime * moveSpeed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }
}
