﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rushScript : MonoBehaviour
{

    public bool phase1, phase2;
    public float speed;
    public GameObject enemy, player;
    
    // Start is called before the first frame update
    void Start()
    {
        phase1 = true;
        phase2 = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (phase1 == true)
        {
            transform.Translate(new Vector3(0, 1, 0) * (speed * Time.deltaTime));
            if (enemy.transform.position.y >= player.transform.position.y) // Compare la position de l'ennemi avec celle du player 
            {
                phase2 = true; // dès que la position de l'ennemi est plus grande ou égale alors il arrête d'aller vers le haut et va vers la gauche
                phase1 = false;
                Debug.Log("position y");
            }
        }

        if (phase2 == true)
        {
            transform.Translate(new Vector3(-1, 0, 0) * (speed * Time.deltaTime));
        }
    }
}
