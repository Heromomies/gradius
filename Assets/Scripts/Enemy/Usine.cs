﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Usine : MonoBehaviour
{
    public GameObject rush;

    public float timeToInvoke, repeatRate, rushFloat;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("LaunchUsine", timeToInvoke, repeatRate); // Invoke des rush 
    }

    void Update()
    {
        if (rushFloat >= 4)
        {
            CancelInvoke("LaunchUsine"); // Si les rush sont supérieurs à 4 alors ca cancel l'invoke
        }
    }

    // Update is called once per frame
    void LaunchUsine()
    {
        Instantiate(rush, transform.position, Quaternion.identity);
        rushFloat++;
    }
}
