﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourEnemy : MonoBehaviour
{
    public float speed;
    public bool isGo, phase1, phase2, phase3;
    public GameObject player, enemy;

    public void Start()
    {
        isGo = false;
        phase1 = true;
        phase2 = false;
        phase3 = false;
    }

    public void Update()
    {
        if (isGo == true && phase1 == true)
        {
            transform.Translate(new Vector3(-1, 0, 0) * (speed * Time.deltaTime)); // il se déplace vers la gauche 
            Debug.Log("non");
            if (gameObject.transform.position.x <= -2)
            {
                phase2 = true;
            }
        }
        if (phase2 == true)
        {
            phase1 = false;
            transform.Translate(new Vector3(0.005f, -0.005f, 0)); // il se déplace en diagonale, dès qu'il a la même position que le joueur il va vers la gauche
            if (Math.Abs(enemy.transform.position.y - player.transform.position.y) < 1)
            {
                phase3 = true;
            }
        }
        if (phase3 == true)
        {
            phase2 = false;
            transform.Translate(new Vector3(1, 0, 0) * (speed * Time.deltaTime)); // va vers la gauche
        }
    }

    public void Go()
    {
        isGo = true;
    }
    
}
