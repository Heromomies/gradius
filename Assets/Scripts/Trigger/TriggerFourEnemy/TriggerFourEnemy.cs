﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFourEnemy : MonoBehaviour
{
   public void OnTriggerEnter2D(Collider2D other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         GetComponent<FourEnemy>().Go();
         Debug.Log("collision");
      }
   }
}
