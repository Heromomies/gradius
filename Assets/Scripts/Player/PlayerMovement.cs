﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;

    private Camera _mainCamera;
    private float _objectWidth;
    private float _objectHeight;
    private Vector2 _screenBounds;
    // Start is called before the first frame update
    void Start()
    {
        _objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
        _objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("UpArrow")) // Déplacement vers le haut
        {
            transform.Translate(new Vector3(0,1 * speed,0) * Time.deltaTime);
        }
        if (Input.GetButton("DownArrow")) // Déplacement vers le bas
        {
            transform.Translate(new Vector3(0,-1 * speed,0) * Time.deltaTime);
        }
        if (Input.GetButton("LeftArrow")) // Déplacement vers la gauche 
        {
            transform.Translate(new Vector3(-1 * speed,0,0) * Time.deltaTime);
        }
        if (Input.GetButton("RightArrow")) // Déplacement vers la droite
        {
            transform.Translate(new Vector3(1 * speed,0,0) * Time.deltaTime);
        }
        _screenBounds = _mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, _mainCamera.transform.position.z));
    }
    
    void LateUpdate() // Le joueur ne pourra pas dépasser l'écran
    {
        Vector3 viewPos = transform.position;
        viewPos.x = Mathf.Clamp(value: viewPos.x, min: _screenBounds.x * -1 + _objectWidth, max: _screenBounds.x - _objectWidth);
        viewPos.y = Mathf.Clamp(value: viewPos.y, min: _screenBounds.y * -1 + _objectHeight, max: _screenBounds.y - _objectHeight);
        transform.position = viewPos;
    }

    public void AddSpeedPlayer()
    {
        speed++;
    }

    public void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (collision2D.gameObject.CompareTag("Bonus"))
        {
            GetComponent<FourEnemy>().Go();
            Debug.Log("Collision player");
        }
    }

    public float SpeedPlayer()
    {
        return speed;
    }
}
