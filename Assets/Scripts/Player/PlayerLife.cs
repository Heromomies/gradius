﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    public float health;
    
    void Update()
    {
        if (health <= 0)
        {
            Die();
        }
    }

    public void Die() // Dès que le joueur meurt, reset les bonus 
    {
        GetComponent<Bonus>().Reset();
        Destroy(gameObject);
    }

    public void Damage()
    {
        health--;
    }
}
