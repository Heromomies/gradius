﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject[] bulletTab, missileTab, laserTab;
    public GameObject missile, bullet, bulletDouble, laser;
    public Transform firePointBullet, firePointMissile, firePointDouble;

    public bool missileBool, doubleBool,laserBool, option;
    public int bonus;
    public GameObject sprite1, sprite2, sprite3, sprite4, sprite5, sprite6;
    void Start()
    {
        missileBool = false;
        doubleBool = false;
        laserBool = false;
        option = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            bonus++;
            Debug.Log(bonus);
        }
        bulletTab = GameObject.FindGameObjectsWithTag("Bullet");
        missileTab = GameObject.FindGameObjectsWithTag("Missile");
        laserTab = GameObject.FindGameObjectsWithTag("Laser");
        if (Input.GetButtonDown("Fire1") && bulletTab.Length < 2 && laserBool == false)
        {
            Instantiate(bullet, firePointBullet.position, Quaternion.identity);
            if (missileBool == true && missileTab.Length < 1)
            {
                Instantiate(missile,new Vector2(firePointMissile.transform.position.x, firePointMissile.transform.position.y), firePointMissile.transform.rotation);
            }
            if (Input.GetButtonDown("Fire1") && doubleBool == true &&  bulletTab.Length < 2)
            {
                Instantiate(bullet, firePointBullet.position, Quaternion.identity);
                Instantiate(bulletDouble, new Vector2(firePointDouble.transform.position.x, firePointDouble.transform.position.y), firePointDouble.transform.rotation);
                if (missileBool == true && missileTab.Length < 1)
                {
                    Instantiate(missile,new Vector2(firePointMissile.transform.position.x, firePointMissile.transform.position.y), firePointMissile.transform.rotation);
                }
            }
        }
        if(Input.GetButtonDown("Fire1") && laserBool == true && laserTab.Length < 4)
        {
            Instantiate(laser, firePointBullet.position, Quaternion.identity);
            if (missileBool == true && missileTab.Length < 1)
            {
                Instantiate(missile,new Vector2(firePointMissile.transform.position.x, firePointMissile.transform.position.y), firePointMissile.transform.rotation);
            }
        }

        if (bonus == 1)
        {
            sprite1.GetComponent<SpriteRenderer>().color = Color.yellow;
            sprite6.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (bonus == 2)
        {
            sprite2.GetComponent<SpriteRenderer>().color = Color.yellow;
            sprite1.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (bonus == 3)
        {
            sprite3.GetComponent<SpriteRenderer>().color = Color.yellow;
            sprite2.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (bonus == 4)
        {
            sprite4.GetComponent<SpriteRenderer>().color = Color.yellow;
            sprite3.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (bonus == 5)
        {
            sprite5.GetComponent<SpriteRenderer>().color = Color.yellow;
            sprite4.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        if (bonus == 6)
        {
            sprite6.GetComponent<SpriteRenderer>().color = Color.yellow;
            sprite5.GetComponent<SpriteRenderer>().color = Color.blue;
        }
        
        if (bonus == 1 && Input.GetButtonDown("ActivateBonus"))
        {
            AddSpeed();
        }
        if (bonus == 2 && Input.GetButtonDown("ActivateBonus"))
        {
            AddMissile();
          
        }
        if (bonus == 3 && Input.GetButtonDown("ActivateBonus"))
        {
            AddDouble();
        }
        if (bonus == 4 && Input.GetButtonDown("ActivateBonus"))
        {
            AddLaser();
        }
        if (bonus == 5 && Input.GetButtonDown("ActivateBonus"))
        {
            AddOption();
        }
        if (bonus == 6 && Input.GetButtonDown("ActivateBonus"))
        {
            AddShield();
        }
        if (bonus > 6)
        {
            bonus = 1;
        }
    }
    public void Missile()
    {
        missileBool = true;
    }

    public void Double()
    {
        doubleBool = true;
    }

    public void Option()
    {
        option = true;
    }
    
    public void AddSpeed()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<PlayerMovement>().AddSpeedPlayer();
        }
    }
    public void AddMissile()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<PlayerAttack>().Missile();
            GetComponent<OptionScript>().Missile();
        }
    }
    public void AddDouble()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<PlayerAttack>().Double();
            GetComponent<OptionScript>().Double();
        }
    }
    public void AddLaser()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<OptionScript>().Laser();
        }
    }
    public void AddOption()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            
        }
    }
    public void AddShield()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
           
        }
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Bonus"))
        {
            bonus++;
        }
    }
}
