﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreScript : MonoBehaviour
{
    public string name;

    void Update()
    {
        GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt(name) + "";
    }
}
