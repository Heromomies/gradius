﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionScript : MonoBehaviour
{
    public float speed;
    public Transform target;
    private Vector2 initD;
    public GameObject[] bulletTab, missileTab, laserTab;
    public GameObject missile, bullet, bulletDouble, laser;
    public Transform firePointBullet, firePointMissile, firePointDouble;

    public bool missileBool, doubleBool,laserBool;

    void Start()
    {
        missileBool = false;
        doubleBool = false;
        laserBool = false;
    }

    // Update is called once per frame
    void Update()
    {
        target = GameObject.Find("Target").transform;
        initD = target.position;
        transform.position = Vector2.MoveTowards(transform.position, initD, speed * Time.deltaTime);
        bulletTab = GameObject.FindGameObjectsWithTag("BulletOption");
        missileTab = GameObject.FindGameObjectsWithTag("MissileOption");
        laserTab = GameObject.FindGameObjectsWithTag("LaserOption");
        if (Input.GetButtonDown("Fire1") && bulletTab.Length < 3 && laserBool == false)
        {
            Instantiate(bullet, firePointBullet.position, Quaternion.identity);
            if (missileBool == true && missileTab.Length < 2)
            {
                Instantiate(missile,new Vector2(firePointMissile.transform.position.x, firePointMissile.transform.position.y), firePointMissile.transform.rotation);
            }
            if (Input.GetButtonDown("Fire1") && doubleBool == true &&  bulletTab.Length < 3)
            {
                Instantiate(bullet, firePointBullet.position, Quaternion.identity);
                Instantiate(bulletDouble, new Vector2(firePointDouble.transform.position.x, firePointDouble.transform.position.y), firePointDouble.transform.rotation);
                if (missileBool == true && missileTab.Length < 2)
                {
                    Instantiate(missile,new Vector2(firePointMissile.transform.position.x, firePointMissile.transform.position.y), firePointMissile.transform.rotation);
                }
            }
        }
        if(Input.GetButtonDown("Fire1") && laserBool == true && laserTab.Length < 5)
        {
            Instantiate(laser, firePointBullet.position, Quaternion.identity);
            if (missileBool == true && missileTab.Length < 1)
            {
                Instantiate(missile,new Vector2(firePointMissile.transform.position.x, firePointMissile.transform.position.y), firePointMissile.transform.rotation);
            }
        }
    }

    public void Missile()
    {
        missileBool = true;
    }

    public void Double()
    {
        doubleBool = true;
    }

    public void Laser()
    {
        laserBool = true;
    }
    
    public void Option()
    {
        
    }
}
