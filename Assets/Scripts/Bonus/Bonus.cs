﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    public int numberBonus;

    public void Update()
    {
        if (numberBonus == 1 && Input.GetButtonDown("ActivateBonus"))
        {
            AddSpeed();
        }
        if (numberBonus == 2 && Input.GetButtonDown("ActivateBonus"))
        {
            AddMissile();
        }
        if (numberBonus == 3 && Input.GetButtonDown("ActivateBonus"))
        {
            AddDouble();
        }
        if (numberBonus == 4 && Input.GetButtonDown("ActivateBonus"))
        {
            AddLaser();
        }
        if (numberBonus == 5 && Input.GetButtonDown("ActivateBonus"))
        {
            AddOption();
        }
        if (numberBonus == 6 && Input.GetButtonDown("ActivateBonus"))
        {
            AddShield();
        }
        /*if (numberBonus > 6)
        {
            numberBonus = 1;
        }*/
    }

    public void AddSpeed()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<PlayerMovement>().AddSpeedPlayer();
        }
    }
    public void AddMissile()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<PlayerAttack>().Missile();
            GetComponent<OptionScript>().Missile();
        }
    }
    public void AddDouble()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<PlayerAttack>().Double();
            GetComponent<OptionScript>().Double();
        }
    }
    public void AddLaser()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            GetComponent<OptionScript>().Laser();
        }
    }
    public void AddOption()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
            
        }
    }
    public void AddShield()
    {
        if (Input.GetButtonDown("ActivateBonus"))
        {
           
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            numberBonus += 1;
            Debug.Log(numberBonus);
            Destroy(gameObject);
        }
    }

    public void Reset()
    {
        //numberBonus = 1;
    }
}
