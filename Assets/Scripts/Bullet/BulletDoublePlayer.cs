﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDoublePlayer : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        //rb.velocity = transform.up * speed;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update() // Ajoute une vélocité à notre balle
    {
        rb.velocity = transform.up * speed;
    }
    private void OnBecameInvisible() //Détruit la bullet si elle ne touche pas de player et qu'elle devient invisible
    {
        Destroy(gameObject);
    }
}
