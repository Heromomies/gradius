﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float speed;
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        //rb.velocity = transform.up * speed;
        rb = GetComponent<Rigidbody2D>();
    }

    void Update() // Ajoute une vélocité à notre balle
    {
        rb.velocity = transform.up * speed;
    }

    public void OnTriggerEnter2D(Collider2D other) // dès que la bullet rentre ne collision avec le player, elle lui inflige un de dégât
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerLife>().Damage();
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible() //Détruit la bullet si elle ne touche pas de player et qu'elle devient invisible
    {
        Destroy(gameObject);
    }
    // Update is called once per frame
}
