﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{
    public float speed;
    private int _damagebullet = 1;

    // Start is called before the first frame update
    void Start()
    {
        _damagebullet = 1;
    }
    void Update() // Ajout vitesse + force 
    {
        transform.Translate(Vector3.right * (speed * Time.deltaTime));
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
