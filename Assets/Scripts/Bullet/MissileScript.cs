﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileScript : MonoBehaviour
{
	public float speed;
	private int _damagebullet = 1;
	private Rigidbody2D rb;

	// Start is called before the first frame update
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		_damagebullet = 1;
	}
	void Update() // Ajout vitesse + force 
	{
		rb.velocity = transform.up * speed;
	}

	private void OnBecameInvisible()
	{
		Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Ground"))
		{
			
		}
	}
}

